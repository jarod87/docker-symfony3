# docker symfony3 env #


* php:7.1-fpm
* nginx:1.11.3
* mysql:5.7
* phpmyadmin/phpmyadmin:latest
* composer:latest


```
#!bash

#(optionall)
composer create-project symfony/framework-standard-edition php/src "3.2.*"

# build images
docker-compose build

#run images
docker-compose up

```


[localhost:80](http://localhost:80/)

[localhost:8888](http://localhost:8888/) (phpmyadmin)
